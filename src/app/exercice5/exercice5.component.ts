import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-exercice5',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './exercice5.component.html',
  styleUrls: ['./exercice5.component.scss']
})
export class Exercice5Component {
  dateCreation: Date = new Date();
  title: string = "Les Gardiens de la Galaxie"
}
