import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-tennisman',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './tennisman.component.html',
  styleUrls: ['./tennisman.component.scss']
})
export class TennismanComponent implements OnChanges {
  @Input({ required: true }) position!: "left" | "right";
  @Input() positionBalle!: "left" | "right";
  @Output() positionBalleChange = new EventEmitter<"left" | "right">();

  url!: string;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['position'] && changes['position'].previousValue !== changes['position'].currentValue) {
      this.url = "/assets/tennisman-" + changes['position'].currentValue + ".png";
    }
    if (changes['positionBalle'] && changes['positionBalle'].previousValue !== changes['positionBalle'].currentValue &&
      this.position === changes['positionBalle'].currentValue
    ) {
      setTimeout(() => {
        const autrePositionBalle = this.positionBalle === "left" ? "right" : "left";
        this.positionBalleChange.emit(autrePositionBalle);
      }, 2000);
    }
  }
}
