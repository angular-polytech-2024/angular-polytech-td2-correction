import { Component } from '@angular/core';
import { TennismanComponent } from './tennisman/tennisman.component';

@Component({
  selector: 'app-exercice3',
  standalone: true,
  imports: [TennismanComponent],
  templateUrl: './exercice3.component.html',
  styleUrls: ['./exercice3.component.scss']
})
export class Exercice3Component {
  positionBalle: 'left' | 'right' = 'left';
}
