import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-portail',
  standalone: true,
  imports: [CommonModule, MatButtonModule],
  templateUrl: './portail.component.html',
  styleUrls: ['./portail.component.scss']
})
export class PortailComponent {
  @Input() id!: number;

  constructor(protected service: SharedService) {}

  getTommy(): void {
    this.service.idWhereIsTommy = this.id;
  }
}
