import { Component } from '@angular/core';
import { PortailComponent } from './portail/portail.component';

@Component({
  selector: 'app-exercice6',
  standalone: true,
  imports: [PortailComponent],
  templateUrl: './exercice6.component.html',
  styleUrls: ['./exercice6.component.scss']
})
export class Exercice6Component {

}
