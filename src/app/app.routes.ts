import { Routes } from '@angular/router';
import { Exercice1Component } from './exercice1/exercice1.component';
import { Exercice2Component } from './exercice2/exercice2.component';
import { Exercice3Component } from './exercice3/exercice3.component';
import { Exercice4Component } from './exercice4/exercice4.component';
import { Exercice5Component } from './exercice5/exercice5.component';
import { Exercice6Component } from './exercice6/exercice6.component';
import { Exercice7Component } from './exercice7/exercice7.component';
import { GoodEndingComponent } from './exercice7/good-ending/good-ending.component';
import { BadEndingComponent } from './exercice7/bad-ending/bad-ending.component';

export const routes: Routes = [
    { path: 'exercice1', component: Exercice1Component },
    { path: 'exercice2', component: Exercice2Component },
    { path: 'exercice3', component: Exercice3Component },
    { path: 'exercice4', component: Exercice4Component },
    { path: 'exercice5', component: Exercice5Component },
    { path: 'exercice6', component: Exercice6Component },
    { path: 'exercice7', component: Exercice7Component, 
      children: [
        { path: 'good', component: GoodEndingComponent },
        { path: 'bad', component: BadEndingComponent }
      ]
     },
    { path: '**', redirectTo: '/exercice1' },
  ];
