import { Component } from '@angular/core';
import { GrandParentComponent } from './grand-parent/grand-parent.component';

@Component({
  selector: 'app-exercice1',
  standalone: true,
  imports: [GrandParentComponent],
  templateUrl: './exercice1.component.html',
  styleUrls: ['./exercice1.component.scss']
})
export class Exercice1Component {
}
