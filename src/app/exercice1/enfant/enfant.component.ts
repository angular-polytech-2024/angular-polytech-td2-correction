import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-enfant',
  standalone: true,
  templateUrl: './enfant.component.html',
  styleUrls: ['./enfant.component.scss']
})
export class EnfantComponent {
  @Input() message: string = '?';
}
