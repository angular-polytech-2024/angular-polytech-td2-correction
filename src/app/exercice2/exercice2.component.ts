import { Component } from '@angular/core';
import { CaisseComponent } from './caisse/caisse.component';

@Component({
  selector: 'app-exercice2',
  standalone: true,
  imports: [CaisseComponent],
  templateUrl: './exercice2.component.html',
  styleUrls: ['./exercice2.component.scss']
})
export class Exercice2Component {

}
