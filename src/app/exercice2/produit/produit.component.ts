import { Component, EventEmitter, Output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-produit',
  standalone: true,
  imports: [MatButtonModule],
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.scss']
})
export class ProduitComponent {
  @Output() selectionProduit: EventEmitter<void> = new EventEmitter<void>();
}
