import { Component, EventEmitter, Output } from '@angular/core';
import { ProduitComponent } from '../produit/produit.component';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-chariot',
  standalone: true,
  imports: [ProduitComponent, MatButtonModule],
  templateUrl: './chariot.component.html',
  styleUrls: ['./chariot.component.scss']
})
export class ChariotComponent {
  @Output() selectionProduits: EventEmitter<number> = new EventEmitter<number>();
  
  count: number = 0;
}
